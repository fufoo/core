package errlib

import (
	"bytes"
	"fmt"
)

func Multiple(l []error) error {
	if len(l) == 0 {
		return nil
	}

	if len(l) == 1 {
		return l[0]
	}

	return MultipleErrors(l)
}

type MultipleErrors []error

func (m MultipleErrors) Error() string {
	var tmp bytes.Buffer
	fmt.Fprintf(&tmp, "%d errors:\n", len(m))
	for _, e := range m {
		tmp.WriteString(e.Error())
		tmp.WriteByte('\n')
	}
	return tmp.String()
}
