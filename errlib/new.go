package errlib

import (
	"fmt"

	"bitbucket.org/dkolbly/logging"
)

type plainMessage string

func (p plainMessage) Error() string {
	return string(p)
}

type errorWithID struct {
	mid string
	msg string
}

// New() is like errors.New() except it includes a message id
func New(id string, message string) errorWithID {
	return errorWithID{
		mid: id,
		msg: message,
	}
}

func (err errorWithID) Error() string {
	// this works with panic, but doesn't work with log.Error()
	return fmt.Sprintf("(%s) %s", err.mid, err.msg)
}

func (err errorWithID) String() string {
	return fmt.Sprintf("%s %s", err.mid, err.msg)
}

func (err errorWithID) Unwrap() error {
	return plainMessage(err.msg)
}

func (err errorWithID) MessageID() string {
	return err.mid
}

func Newf(msg string, args ...interface{}) errorWithID {
	pre, post := logging.Split(msg)
	if pre == "" {
		panic("must have msgid for errlib.New()")
	}
	return New(pre, fmt.Sprintf(post, args...))
}
