package errlib

import (
	"fmt"
)

type ErrorWithID interface {
	error
	MessageID() string
	Unwrap() error
}

type Wrapped struct {
	mid  string
	next error
}

func (w Wrapped) MessageID() string {
	return w.mid
}

func (w Wrapped) Unwrap() error {
	return w.next
}

func (w Wrapped) Error() string {
	return fmt.Sprintf("(%s) λ %s", w.mid, w.next.Error())
}

func Wrap(mid string, next error) ErrorWithID {
	return Wrapped{
		mid:  mid,
		next: next,
	}
}
