package hexdump

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

const LineSize = 16

// A dumps the data to stdout
func A(data []byte) {
	WithASCII(os.Stdout, data)
}

// WithASCII dumps the data to the output in both hex and ascii form
func WithASCII(dst io.Writer, data []byte) {
	out := bufio.NewWriter(dst)
	defer out.Flush()

	offset := 0

	for len(data) > 0 {
		fmt.Fprintf(out, "%04x:", offset)

		for i := 0; i < LineSize; i++ {
			if i%2 == 0 {
				out.WriteByte(' ')
			}
			if i < len(data) {
				fmt.Fprintf(out, "%02x", data[i])
			} else {
				out.WriteString("  ")
			}
		}
		out.WriteByte(' ')

		n := 0
		for i := 0; i < LineSize; i++ {
			if i >= len(data) {
				break
			}
			n++
			offset++
			if data[i] >= ' ' && data[i] <= '~' {
				out.WriteByte(data[i])
			} else {
				out.WriteByte('.')
			}
		}
		out.WriteByte('\n')
		data = data[n:]
	}
}
