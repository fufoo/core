package hexdump

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestASCII(t *testing.T) {
	var a, b, c, d, e strings.Builder

	WithASCII(&a, []byte{0x41})
	assert.Equal(t, "0000: 41                                      A\n", a.String())

	WithASCII(&b, []byte{0x41, 1, 0x20, 0x21})
	assert.Equal(t, "0000: 4101 2021                               A. !\n", b.String())

	WithASCII(&c, []byte{0x41, 0x7e, 0x7f})
	assert.Equal(t, "0000: 417e 7f                                 A~.\n", c.String())

	WithASCII(&d, []byte{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'})
	assert.Equal(t, "0000: 3031 3233 3435 3637 3839 4142 4344 4546 0123456789ABCDEF\n", d.String())

	WithASCII(&e, []byte{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'x'})
	assert.Equal(t,
		"0000: 3031 3233 3435 3637 3839 4142 4344 4546 0123456789ABCDEF\n"+
			"0010: 78                                      x\n", e.String())
}
