package version

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"time"
)

var V Version

type Version struct {
	Version   string    `json:"version"`
	BuildTime time.Time `json:"build_time"`
	Commit    string    `json:"commit,omitempty"`
	Branch    string    `json:"branch,omitempty"`
	Tag       string    `json:"tag,omitempty"`
	Name      string    `json:"name"`
}

func (v Version) String() string {
	if v.Version != "" {
		return v.Version
	}
	if v.Tag != "" {
		return v.Tag
	}
	if v.BuildTime.IsZero() {
		return fmt.Sprintf("%.7s", v.Commit)
	}

	return fmt.Sprintf("%.7s-%s", v.Commit, v.BuildTime.Format("150405"))
}

func Init(ticket string) error {
	if ticket == "" {
		return nil
	}

	buf, err := base64.StdEncoding.DecodeString(ticket)
	if err != nil {
		return err
	}

	err = json.Unmarshal(buf, &V)
	if err != nil {
		return err
	}
	return nil
}
