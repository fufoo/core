package memo

import (
	"bytes"
	"context"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
)

var log = logging.New("memo")

// Able is something that is a value (the result of a computation)
// that is able to be memoized.  That is, it is memo.Able.
type Able interface {
	Encode() ([]byte, error)
	Decode([]byte) error
}

type Arg interface {
	Hash() string
}

type Store interface {
	// should return nil if the item is not found, or an error
	// occurs
	Get(context.Context, string) []byte
	// don't bother returning an error; this is a best effort
	// protocol; if storing fails, we'll just have to recompute
	// next time
	Put(context.Context, string, []byte)
}

type Func interface {
	Name() string
	Apply(context.Context, ...Arg) (Able, error)
	Alloc() Able
}

type Config struct {
	Read  bool
	Write bool
	Store Store
}

var DefaultConfig = &Config{}

func Init(s Store) {
	DefaultConfig.Store = s
	DefaultConfig.Read = true
	DefaultConfig.Write = true
}

func Ize(ctx context.Context, f Func, args ...Arg) (Able, error) {
	return DefaultConfig.Ize(ctx, f, args...)
}

func (c *Config) Ize(ctx context.Context, f Func, args ...Arg) (Able, error) {
	var key string

	if c.Read || c.Write {
		var kb bytes.Buffer
		kb.WriteString(f.Name())
		kb.WriteByte('(')
		for i, arg := range args {
			if i > 0 {
				kb.WriteByte(',')
			}
			kb.WriteString(arg.Hash())
		}
		kb.WriteByte(')')
		key = kb.String()
	}

	if c.Read {
		value := c.Store.Get(ctx, key)
		if value != nil {
			log.Debugf(ctx, "FUT-5801 hit %s", key)
			log.Debugf(ctx, "%d bytes %.30x", len(value), value)
			tmp := f.Alloc()
			err := tmp.Decode(value)
			if err != nil {
				return nil, errlib.Wrap("FUT-5899", err)
			}
			return tmp, nil
		}
	}

	if c.Read || c.Write {
		log.Debugf(ctx, "FUT-5802 miss %s", key)
	}
	value, err := f.Apply(ctx, args...)
	if err != nil {
		return nil, err
	}
	if c.Write {
		buf, err := value.Encode()
		if err != nil {
			// this should never happen unless our memoization
			// client application is busted
			panic(err)
		}
		log.Debugf(ctx, "FUT-5803 store %d bytes for %s", len(buf), key)
		c.Store.Put(ctx, key, buf)
	}
	return value, nil
}
