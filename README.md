# FuFoo Core

This repository provides shared services and libraries across the
**FuFoo** system.  It does not have any dependencies within the
system, and in some cases wraps functionality imported from other
external dependencies.

## Libraries

* **version** - provides introspection on the version of executables
  using a base64 encoded build ticket.

* **ptr** - low level foo pointer type, placed here in `core` (instead
  of in `foo`) so that `fulang` can make use of it for its caching
  compiler.

* **memo** - a memoization helper library

* **errlib** - utilities for better errors (most notably with message ids)

* **list** - a generic list implementation with a `container/list` type API

* **hexdump** - printing out data in hex _a la_ `xxd`

* **tabular** - table output
