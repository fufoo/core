package list

// List is a circular linked list.
type List[T any] struct {
	free *Node[T]
	head *Node[T]
}

// Range iterates over the values in the list, in order from front to
// back.  If the provided function returns non-nil, then iteration
// stops and that error is returned from this function.
func (l *List[T]) Range(fn func(T) error) error {
	if l.head == nil {
		return nil
	}
	n := l.head
	for {
		err := fn(n.Value)
		if err != nil {
			return err
		}
		n = n.succ
		if n == l.head {
			return nil
		}
	}
}

// RangeReverse iterates over the values in the list, in order from
// back to front (i.e., in reverse).  If the provided function returns
// non-nil, then iteration stops and that error is returned from this
// function.
func (l *List[T]) RangeReverse(fn func(T) error) error {
	if l.head == nil {
		return nil
	}
	start := l.head.pred
	n := start
	for {
		err := fn(n.Value)
		if err != nil {
			return err
		}
		n = n.pred
		if n == start {
			return nil
		}
	}
}

// Node represents a value in a linked list
type Node[T any] struct {
	pred  *Node[T]
	succ  *Node[T]
	Value T
}

// Front returns the node at the front of the list, or nil
// if the list is empty.
func (l *List[T]) Front() *Node[T] {
	return l.head
}

// Back returns the node at the back of the list, or nil if
// the list is empty
func (l *List[T]) Back() *Node[T] {
	if l.head == nil {
		return nil
	}
	return l.head.pred
}

// IsEmpty returns true if the list has no elements
func (l *List[T]) IsEmpty() bool {
	return l.head == nil
}

// PopFront removes the front element from the list and returns its
// value
func (l *List[T]) PopFront() T {
	return l.Remove(l.head)
}

func (l *List[T]) alloc(x T) *Node[T] {
	n := l.free
	if n == nil {
		n = new(Node[T])
	} else {
		l.free = n.succ
	}
	n.Value = x
	return n
}

// PushBack adds an element to the back of the list and returns its
// node
func (l *List[T]) PushBack(x T) *Node[T] {
	t := l.alloc(x)
	h := l.head
	if h == nil {
		t.pred, t.succ = t, t
		l.head = t
	} else {
		t.pred, t.succ = h.pred, h
		t.pred.succ = t
		h.pred = t
	}
	return t
}

// PushFront adds an element to the front of the list and returns its
// node
func (l *List[T]) PushFront(x T) *Node[T] {
	n := l.alloc(x)
	h := l.head
	if h == nil {
		n.pred, n.succ = n, n
		l.head = n
	} else {
		t := h.pred
		n.succ, h.pred = h, n
		n.pred, t.succ = t, n
		l.head = n
	}
	return n
}

// MoveToFront moves the node n to the front of the list l.
// The node must not be nil.
func (l *List[T]) MoveToFront(n *Node[T]) {
	// as a special case, if the node is at the *end* of the list,
	// all we have to do is adjust the head pointer
	if l.head.pred == n {
		l.head = n
		return
	}
	// as another special case, if the node is *already* at the start
	// of the list, we need do nothing
	if l.head == n {
		return
	}
	// as a third special case, if the list only has one element,
	// then we need do nothing
	if n.succ == n {
		panic("this should be caught by the other cases")
	}
	// at this point, we know the list has at least one other element,
	// so we can pop the node out and install it as the new head
	l.move(n, l.head.pred)
	l.head = n // we are the new head

}

// MoveToFront moves the node n to the front of the list l.
// The node must not be nil.
func (l *List[T]) MoveToBack(n *Node[T]) {
	// as a special case, if the node is at the *start* of the list,
	// all we have to do is adjust the head pointer
	if l.head == n {
		l.head = n.succ
		return
	}
	// as another special case, if the node is *already* at the end
	// of the list, we need do nothing
	if l.head.pred == n {
		return
	}
	// as a third special case, if the list only has one element,
	// then we need do nothing
	if n.succ == n {
		panic("this should be caught by the other cases")
	}
	// at this point, we know the list has at least one other element,
	// so we can pop the node out and install it after the former tail
	// of the list
	l.move(n, l.head.pred)
}

// remove a node, returning its value
func (l *List[T]) Remove(n *Node[T]) (x T) {
	if n.succ == n {
		// there is only one element in the list, so just zero out the
		// head
		l.head = nil
	} else {
		if n == l.head {
			// we are the head item item, so go to the next one
			l.head = n.succ
		}
		// unlink it from the list
		n.unlink()
	}
	// put it on the free list
	n.succ = l.free
	n.pred = nil
	l.free = n

	// zero out the data, in case it needs GCing
	x = n.Value
	var zero T
	n.Value = zero

	return
}

func (n *Node[T]) unlink() {
	n.pred.succ = n.succ
	n.succ.pred = n.pred
}

// move n to after at
func (l *List[T]) move(n, at *Node[T]) {
	if n == at {
		return
	}
	// remove us from where we are
	n.unlink()

	// install node `n` just after `at`
	n.pred = at
	n.succ = at.succ
	n.pred.succ = n
	n.succ.pred = n
}

func (l *Node[T]) Next() *Node[T] {
	return l.succ
}

func (l *Node[T]) Prev() *Node[T] {
	return l.pred
}
