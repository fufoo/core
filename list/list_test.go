package list

import (
	"container/list"
	"fmt"
	"strings"
	"testing"
)

// a command is a space-separate list of one- or two-letter commands
//    hx  push x onto head of list
//    tx  push x onto tail of list
//    Fx  move x to the front of the list
//    H  pop head of list
//    T  pop tail of list
// at the end, we compare the order of items to the reference list
// implementation (from collections/list)

func run(t *testing.T, cmd string) {
	fmt.Printf("---------- %q\n", cmd)
	l := new(List[byte])
	qc := list.New()

	qcreport := func() string {
		var result string
		a := qc.Front()
		for a != nil {
			result += string(a.Value.(byte))
			a = a.Next()
		}
		return result
	}

	lreport := func() string {
		if l.head == nil {
			return ""
		}

		var result string
		a := l.head
		for {
			result += string(a.Value)
			a = a.succ
			if a == l.head {
				break
			}
		}
		return result
	}

	qcfind := func(x byte) *list.Element {
		a := qc.Front()
		for a != nil {
			if x == a.Value {
				return a
			}

			a = a.Next()
		}
		t.Fatalf("could not find %q element in qc list", x)
		return nil
	}

	lfind := func(x byte) *Node[byte] {
		a := l.head
		for a != nil {
			if a.Value == x {
				return a
			}
			a = a.succ
		}
		t.Fatalf("could not find %q in test list", x)
		return nil
	}

	i := 0
	for i < len(cmd) {
		switch cmd[i] {
		case 't':
			i++
			x := cmd[i]

			l.PushBack(x)
			qc.PushBack(x)

		case 'h':
			i++
			x := cmd[i]

			l.PushFront(x)
			qc.PushFront(x)

		case 'H':
			x := l.PopFront()
			y := qc.Remove(qc.Front())
			if x != y {
				t.Fatalf("expected head to be %q but was %q", x, y)
			}

		case 'F':
			i++
			x := cmd[i]
			qp := qcfind(x)
			lp := lfind(x)

			l.MoveToFront(lp)
			qc.MoveToFront(qp)

		case 'B':
			i++
			x := cmd[i]
			qp := qcfind(x)
			lp := lfind(x)

			l.MoveToBack(lp)
			qc.MoveToBack(qp)

		default:
			panic("bad run")
		}
		i++
		if i < len(cmd) {
			if cmd[i] != ' ' {
				panic("missing space")
			}
			i++
		}
	}

	// make sure we match the reference impl
	fmt.Printf("reference state: |%s|\n", qcreport())
	fmt.Printf("test state:      |%s|\n", lreport())

	a := qc.Front()
	k := 0
	for a != nil {
		x := l.PopFront()
		fmt.Printf("got [%d] %#v\n", k, x)
		if x != a.Value {
			t.Errorf("expected element [%d] to be %q but was %q",
				k,
				a.Value,
				x,
			)
		}
		k++
		a = a.Next()
	}
}

func TestOps(t *testing.T) {
	run(t, "t0 t1 t2")
	run(t, "t0 t1 H t2")

	run(t, "t0 t1 t2 F0")
	run(t, "t0 t1 t2 F1")
	run(t, "t0 t1 t2 F2")
	run(t, "t0 F0")
	run(t, "t0 t1 F0")
	run(t, "t0 t1 F1")

	run(t, "t0 t1 t2 B0")
	run(t, "t0 t1 t2 B1")
	run(t, "t0 t1 t2 B2")
	run(t, "t0 t1 t2 B2")
	run(t, "t0 B0")
	run(t, "t0 t1 B0")
	run(t, "t0 t1 B1")

	run(t, "t0 t1 t2")
	run(t, "t0 t1 H t2")

	run(t, "h0 h1 h2 F0")
	run(t, "h0 h1 h2 F1")
	run(t, "h0 h1 h2 F2")
	run(t, "h0 F0")
	run(t, "h0 h1 F0")
	run(t, "h0 h1 F1")

	run(t, "h0 h1 h2 B0")
	run(t, "h0 h1 h2 B1")
	run(t, "h0 h1 h2 B2")
	run(t, "h0 h1 h2 B2")
	run(t, "h0 B0")
	run(t, "h0 h1 B0")
	run(t, "h0 h1 B1")

	run(t, "h0 t1 h2 F0")
	run(t, "t0 h1 t2 F0")
}

func TestRange(t *testing.T) {
	var l List[string]

	t.Run("range empty", func(t *testing.T) {
		n := 0
		l.Range(func(x string) error {
			n++
			return nil
		})
		if n != 0 {
			t.Errorf("expected to find 0, but found %d", n)
		}
	})

	l.PushBack("a")
	l.PushBack("b")
	l.PushBack("c")

	t.Run("range all", func(t *testing.T) {
		var found []string
		l.Range(func(x string) error {
			found = append(found, x)
			return nil
		})
		if len(found) != 3 {
			t.Errorf("expected to find 3, but found %d", len(found))
		}
		fj := strings.Join(found, " ")
		if fj != "a b c" {
			t.Errorf("expected to find \"a b c\", but found %q", fj)
		}
	})

	t.Run("range some", func(t *testing.T) {
		var found []string
		l.Range(func(x string) error {
			found = append(found, x)
			if x == "b" {
				return fmt.Errorf("stop")
			}
			return nil
		})
		if len(found) != 2 {
			t.Errorf("expected to find 2, but found %d", len(found))
		}
		fj := strings.Join(found, " ")
		if fj != "a b" {
			t.Errorf("expected to find \"a b\", but found %q", fj)
		}
	})
}

func TestRangeReverse(t *testing.T) {
	var l List[string]

	t.Run("range empty", func(t *testing.T) {
		n := 0
		l.RangeReverse(func(x string) error {
			n++
			return nil
		})
		if n != 0 {
			t.Errorf("expected to find 0, but found %d", n)
		}
	})

	l.PushBack("a")
	l.PushBack("b")
	l.PushBack("c")

	t.Run("range all", func(t *testing.T) {
		var found []string
		l.RangeReverse(func(x string) error {
			found = append(found, x)
			return nil
		})
		if len(found) != 3 {
			t.Errorf("expected to find 3, but found %d", len(found))
		}
		fj := strings.Join(found, " ")
		if fj != "c b a" {
			t.Errorf("expected to find \"c b a\", but found %q", fj)
		}
	})

	t.Run("range some", func(t *testing.T) {
		var found []string
		l.RangeReverse(func(x string) error {
			found = append(found, x)
			if x == "b" {
				return fmt.Errorf("stop")
			}
			return nil
		})
		if len(found) != 2 {
			t.Errorf("expected to find 2, but found %d", len(found))
		}
		fj := strings.Join(found, " ")
		if fj != "c b" {
			t.Errorf("expected to find \"c b\", but found %q", fj)
		}
	})
}
