module bitbucket.org/fufoo/core

go 1.18

require (
	bitbucket.org/dkolbly/logging v0.9.6
	github.com/google/uuid v1.6.0
	github.com/stretchr/testify v1.8.4
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20190124100055-b90733256f2e // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
