package ptr

// note, this encoding is deliberately chosen so that the sort order
// for the stringform is the same as the sort order for the underlying
// binary representation.

// String returns the string representation of a ptr, which is a
// base62 encoding of 251 bits, followed by a letter based on the kind
// (which is 5 bits).  The metatype code is 0-31, only some of which
// are used.
func (p Ptr) String() string {
	var out [44]byte

	if p.Kind() == 0 {
		// this will sort in ascii before the smallest valid value because
		// we start numbering the high digit at 'a', so all upper case
		// letters sort earlier
		return "NULL"
	}
	if !p.IsValid() {
		return "INVALID"
	}

	group := func(i int) uint64 {
		var group uint64

		for j := 0; j < 8; j++ {
			group += uint64(p.Bits[i*8+j]) << (64 - 8*(j+1))
		}
		return group
	}

	// for the last group, take out the low 5 bits because for the
	// last group, take out the low 5 bits because are expressed
	// in the metatype code, and as a result it only produces 10
	// digits in base 62 (because 2^(64-5) =~ 5.8E+17 which fits
	// within 62^10 =~ 8.4E+17).  We do this group first so that
	// it can write its '0' to out[32], that will be overwritten
	// in the next group

	base62(out[32:43], group(3)>>5, false)
	base62(out[22:33], group(2), true)
	base62(out[11:22], group(1), true)
	base62(out[0:11], group(0), true)
	out[43] = p.Kind().kindchar()
	return string(out[:])
}

const encoding62 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

func base62(dst []byte, x uint64, remainder bool) {
	for i := 0; i < 10; i++ {
		dst[10-i] = encoding62[x%62]
		x /= 62
	}
	// the remainder should be <= 21, which is great because we want to make
	// sure we don't start with a digit if possible
	if x > 21 {
		panic("miscalculated")
	}
	if remainder {
		dst[0] = 'a' + byte(x)
	}
}

func unbase62(src string, remainder bool) (uint64, bool) {
	var accum uint64

	for i := 0; i < len(src); i++ {
		ch := src[i]
		accum *= 62
		if ch >= '0' && ch <= '9' {
			accum += uint64(ch - '0')
		} else if ch >= 'A' && ch <= 'Z' {
			accum += uint64(ch - 'A' + 10)
		} else if ch >= 'a' && ch <= 'z' {
			if i == 0 && remainder {
				accum += uint64(ch - 'a')
			} else {
				accum += uint64(ch - 'a' + 10 + 26)
			}
		} else {
			return 0, false
		}
	}
	return accum, true
}

func (p *Ptr) SetString(src string) bool {
	if src == "NULL" {
		*p = Null
		return true
	}

	if len(src) != 44 {
		return false
	}
	k, ok := char2kind(src[43])
	if !ok {
		return false
	}

	ungroup := func(i int, x uint64) {
		for j := 0; j < 8; j++ {
			p.Bits[i*8+j] = byte(x >> (64 - 8*(j+1)))
		}
	}

	v, ok := unbase62(src[0:11], true)
	if !ok {
		return false
	}
	ungroup(0, v)

	v, ok = unbase62(src[11:22], true)
	if !ok {
		return false
	}
	ungroup(1, v)

	v, ok = unbase62(src[22:33], true)
	if !ok {
		return false
	}
	ungroup(2, v)

	v, ok = unbase62(src[33:43], false)
	if !ok {
		return false
	}
	ungroup(3, uint64(k)+(v<<5))
	return true
}

/*

enc="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

def base62(x):
  if x == 0:
    return '0'
  else:
    return base62(x/62) + enc[x%62]

def unbase62(src):
  result = 0L
  for ch in src:
    result = (result * 62) + enc.index(ch)
  return result

*/
