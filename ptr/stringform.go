package ptr

import (
	"errors"
)

// note that in base 62, we need 42 characters to encode 2^248 which
// means if we just drop the whole last byte (instead of sharing 4
// bits with the metatype), things are simpler... except of course
// that we need to use bignum to convert back and forth :)

// Decode a string-encoded Ptr (old api)
func DecodeStringPtr(s string) (p Ptr, ok bool) {
	ok = p.SetString(s)
	return
}

/*func (p Ptr) Encode(dest []byte) (int, error) {
	copy(dest, p.Bits[:])
	return PtrSize, nil
}

func (p *Ptr) Decode(buf []byte) (int, error) {
	mtc := mtToChar(MetatypeCode(buf[31] & metatypeMask))
	if mtc == '?' {
		return 0, ErrInvalidMetatype
	}

	copy(p.Bits[:], buf)
	return PtrSize, nil
}
*/

var ErrInvalidPtr = errors.New("invalid ptr")

func (p *Ptr) UnmarshalJSON(src []byte) error {
	// unmarshal an empty string as the null ptr
	if len(src) == 2 && src[0] == '"' && src[1] == '"' {
		*p = Null
		return nil
	}

	var d Ptr
	ok := d.SetString(string(src[1 : len(src)-1]))
	if !ok {
		return ErrInvalidPtr
	}

	*p = d
	return nil
}

func (p Ptr) MarshalJSON() ([]byte, error) {

	// marshal the null ptr as the empty string
	if p.IsNull() {
		return []byte(`""`), nil
	}

	if !p.IsValid() {
		return nil, ErrInvalidPtr
	}
	str := `"` + p.String() + `"`
	return []byte(str), nil
}
