package ptr

import (
	"fmt"
	"testing"

	"github.com/google/uuid"
)

func TestUUID(t *testing.T) {
	u := uuid.New()
	p := UUID(u)
	if p.LiteralString() != u.String() {
		t.Errorf("expected uuid to match, but %q != %q",
			p.LiteralString(),
			u.String(),
		)
	}
	fmt.Printf("got %q\n", p.LiteralString())
}
