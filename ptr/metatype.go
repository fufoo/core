package ptr

import (
	"errors"
	"fmt"
)

var ErrInvalidMetatype = errors.New("invalid ptr type")

const kindMask = 0x1f
const kindBits = 5

type Kind byte

// if these are not in ascii order, then TestSortOrder
// will fail... our range is 32, so leave a lot of space
// so we could add new kinds later and not break all existing
// pointers
const (
	LiteralBytes   = Kind(11) //  'b'  (also 0xb, interestingly; not planned)
	Encrypted      = Kind(12) //  'e'  (0xc)
	Hash           = Kind(14) //  'h'  (0xe)
	LiteralUUID    = Kind(15) //  'i'  (0xf)
	LiteralRat     = Kind(22) //  'n'  (0x16)
	LiteralString  = Kind(26) //  's'  (0x1a)
	HasTypePointer = Kind(27) //  't'  (0x1b)
	Untyped        = Kind(28) //  'u'  (0x1c)
)

func (p Ptr) Kind() Kind {
	return Kind(p.Bits[31] & kindMask)
}

func (p *Ptr) setkind(k Kind) {
	p.Bits[31] = (p.Bits[31] &^ kindMask) + (byte(k) & kindMask)
}

var mtEncoding = [1 << kindBits]byte{
	// *NOTE* these encodings are specifically chosen
	// so that the sort order in ascii is the same
	// as the sort order of the encoding (for valid encodings at least).
	//
	// we want to preserve the invariant that
	// if P1 < P2 then P1.String() < P2.String()
	// (c.f. TestSortOrder)
	Untyped:        'u',
	HasTypePointer: 't',
	Encrypted:      'e',
	Hash:           'h',
	LiteralRat:     'n',
	LiteralBytes:   'b',
	LiteralString:  's',
	LiteralUUID:    'i',
}

func (k Kind) kindchar() byte {
	return mtEncoding[k]
}

func char2kind(ch byte) (Kind, bool) {
	switch ch {
	case 'u':
		return Untyped, true
	case 't':
		return HasTypePointer, true
	case 'e':
		return Encrypted, true
	case 'h':
		return Hash, true
	case 'n':
		return LiteralRat, true
	case 'i':
		return LiteralUUID, true
	case 'b':
		return LiteralBytes, true
	case 's':
		return LiteralString, true
	default:
		return 0, false
	}
}

// this returns the shell type name for the kind
func (k Kind) TypeName() string {
	switch k {
	case LiteralBytes:
		return "bytes"
	case Encrypted:
		return "encrypted"
	case Hash:
		return "hash"
	case LiteralUUID:
		return "uuid"
	case LiteralRat:
		return "rat"
	case LiteralString:
		return "string"
	case Untyped:
		return "blob"
	case HasTypePointer:
		return "obj"
	default:
		return fmt.Sprintf("ptr.Kind(%d)", k)
	}
}

func (k Kind) String() string {
	switch k {
	case LiteralBytes:
		return "LiteralBytes"
	case Encrypted:
		return "Encrypted"
	case Hash:
		return "Hash"
	case LiteralUUID:
		return "LiteralUUID"
	case LiteralRat:
		return "LiteralRat"
	case LiteralString:
		return "LiteralString"
	case Untyped:
		return "Untyped"
	case HasTypePointer:
		return "HasTypePointer"
	default:
		return fmt.Sprintf("ptr.Kind(%d)", k)
	}
}
