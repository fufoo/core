package ptr

import (
	"fmt"
	"testing"
)

// make sure that the sort order for the binary data is the same
// as the sort order for the ascii data
func TestSortOrder(t *testing.T) {

	// this loop arranges to cycle every byte, such that we go
	// through the Ptr's in ascending binary order
	prev := ""

	emit := func(p Ptr) {
		this := p.String()
		if p.String() <= prev {
			t.Errorf("expected monotonically increasing, but %q <= %q",
				this,
				prev)
		}
		prev = this
	}

	emit(Null) // the loop below does not actually generate Null
	// because it is not valid

	for i := 31; i >= 0; i-- {
		for j := 1; j < 256; j++ {
			var z Ptr
			z.Bits[i] = byte(j)
			if i != 31 {
				z.Bits[31] = byte(Hash)
			}
			if z.IsValid() {
				fmt.Printf("%02d %03d [%x] -> %s\n", i, j, z.Bits[:], z.String())
				emit(z)
			}
		}
	}
}

func TestRoundTrip(t *testing.T) {
	var tests = []Ptr{
		Null,
		String("hello"),
		Int64(102345),
		Int64(-1),
		Bytes([]byte("hello")),
		HashAddress([]byte("hello")),
		TypedContentAddress(String("T"), []byte("hello")),
		UntypedContentAddress([]byte("hello")),
		EncryptedContentAddress([]byte("my-secret-string")),
	}

	for i, p := range tests {
		t.Run(fmt.Sprintf("%d", i),
			func(t *testing.T) {
				fmt.Printf("--> %s [%x]\n", p.String(), p.Bits[:])
				str := p.String()
				p2, ok := DecodeStringPtr(str)
				if !ok {
					t.Errorf("expected to parse %q, but didn't", str)
				} else if p != p2 {
					t.Errorf("expected to round trip to match, but [%x] != [%x]", p.Bits[:], p2.Bits[:])
				}
			})
	}
}

func BenchmarkEncode(b *testing.B) {
	var tests = []Ptr{
		String("hello"),
		Int64(102345),
		Int64(-1),
		Bytes([]byte("hello")),
		HashAddress([]byte("hello")),
		TypedContentAddress(String("T"), []byte("hello")),
		UntypedContentAddress([]byte("hello")),
	}

	for i := 0; i < b.N; i++ {
		// 187 ns/op
		tests[i%len(tests)].String()
	}
}

func BenchmarkDecode(b *testing.B) {
	var tests = []string{
		"i4GKd1BT9xYc0nhtsyVVWycNNqBLRmGjtAnkh2vUFdts",
		"iA0XxTmuNfOuyEdbsTvxlTeLHYOWobU5qSXpZSMRed6n",
		"rxMAYJ8eroRrA6bgY6ua3np0H3lDCWBehVMNiXqmsXgn",
		"i4GKd1BT9xYc0nhtsyVVWycNNqBLRmGjtAnkh2vUFdtb",
		"drFRl63gZfqdL6JYReWypqcKBAyjLaWgYJ8C5LkJJMvh",
		"qmGhfYdjJyDcYep0JRENrRai41Zw9H1NoKlkiQZvzbot",
		"drFRl63gZfqdL6JYReWypqcKBAyjLaWgYJ8C5LkJJMvu",
	}

	// 157 ns/op
	for i := 0; i < b.N; i++ {
		var p Ptr
		p.SetString(tests[i%len(tests)])
	}
}
