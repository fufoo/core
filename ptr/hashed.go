package ptr

import (
	"crypto/sha256"
)

func EncryptedContentAddress(data []byte) (p Ptr) {
	return contentAddress(Encrypted, data)
}

func UntypedContentAddress(data []byte) (p Ptr) {
	return contentAddress(Untyped, data)
}

func HashAddress(body []byte) (p Ptr) {
	return contentAddress(Hash, body)
}

func contentAddress(k Kind, data []byte) (p Ptr) {
	h := sha256.Sum256(data)
	return FromHash(k, h[:])
}

// TypedContentAddress computes the Ptr value for a body
// with the given type header.  The resulting Ptr has
// metatype HasTypePointer
func TypedContentAddress(t Ptr, body []byte) (p Ptr) {
	h := sha256.New()
	h.Write(t.Bits[:])
	h.Write(body)
	return FromHash(HasTypePointer, h.Sum(nil))
}

func FromHash(kind Kind, h []byte) (p Ptr) {
	if len(h) != sha256.Size {
		panic("hash data wrong size")
	}
	copy(p.Bits[:], h[:])
	p.Bits[31] = (p.Bits[31] &^ kindMask) + byte(kind)
	return p
}
