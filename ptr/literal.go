package ptr

import (
	"crypto/aes"
	"crypto/cipher"
	"fmt"
	"math/big"
	"strconv"
)

// literals have 30 bytes to store most of their data, and (16-metatypeBits) bits to store
// control information which is stored in the last two bytes.

const MaxStringLength = 30

func (p *Ptr) low16() uint16 {
	return (uint16(p.Bits[30]) << 8) + uint16(p.Bits[31])
}

func (p *Ptr) setlow16(x uint16) {
	p.Bits[30] = byte(x >> 8)
	p.Bits[31] = byte(x)
}

func (p *Ptr) control() uint16 {
	return p.low16() >> kindBits
}

func (p *Ptr) setcontrol(value uint16) {
	p.setlow16((p.low16() & kindMask) + (value << kindBits))
}

const obfuscateLiterals = true

func unobscure(p *Ptr) {
	if obfuscateLiterals {
		right := p.Bits[30-16 : 30]
		left := p.Bits[0:16]
		literalCipher.Decrypt(left, left)
		literalCipher.Decrypt(right, right)
		literalCipher.Decrypt(left, left)
	}
}

func obscure(p *Ptr) {
	if obfuscateLiterals {
		right := p.Bits[30-16 : 30]
		left := p.Bits[0:16]
		literalCipher.Encrypt(left, left)
		literalCipher.Encrypt(right, right)
		literalCipher.Encrypt(left, left)
	}
}

var literalCipher cipher.Block

func init() {
	literalKey := [24]byte{
		0x49, 0xc7, 0x3f, 0x94, 0xe1, 0x23, 0x29, 0x5f,
		0xec, 0xa5, 0xfb, 0xad, 0xd5, 0x94, 0xe7, 0xd6,
	}
	c, err := aes.NewCipher(literalKey[:])
	if err != nil {
		panic(err)
	}
	literalCipher = c
}

func (p Ptr) Uint64Value() uint64 {
	r := p.RatValue()
	if !r.IsInt() {
		panic("not an integer literal")
	}
	i := r.Num()
	if !i.IsUint64() {
		panic("not a small integer")
	}
	return i.Uint64()
}

func (p Ptr) IntValue() *big.Int {
	r := p.RatValue()
	if !r.IsInt() {
		panic("not an integer literal")
	}
	return r.Num()
}

// CONTROL INFORMATION
//
// 11       10                      5                         0
// +--------+-----------------------+-------------------------+
// |  SIGN  |  NUM NUMERATOR BYTES  |  NUM DENOMINATOR BYTES  |
// +--------+-----------------------+-------------------------+

func (p Ptr) RatValue() *big.Rat {
	if !p.IsRat() {
		panic("not a rational literal")
	}
	unobscure(&p)
	ctl := p.control()
	neg := (ctl >> 10) != 0
	nbytes := (ctl >> 5) & 0x1f
	dbytes := ctl & 0x1f

	answer := new(big.Rat)

	var num, den big.Int

	num.SetBytes(p.Bits[0:nbytes])
	if dbytes == 0 {
		den.SetUint64(1)
	} else {
		den.SetBytes(p.Bits[nbytes : nbytes+dbytes])
	}
	if neg {
		num.Neg(&num)
	}
	answer.SetFrac(&num, &den)

	return answer
}

// CONTROL INFORMATION
//
// 11       10                      5                         0
// +--------+-----------------------+-------------------------+
// |                                |         LENGTH          |
// +--------+-----------------------+-------------------------+

func (p Ptr) IsString() bool {
	return p.Kind() == LiteralString
}

func (p Ptr) IsBytes() bool {
	return p.Kind() == LiteralBytes
}

func (p Ptr) IsRat() bool {
	return p.Kind() == LiteralRat
}

func (p Ptr) IsInt() bool {
	return p.IsRat() && p.RatValue().IsInt()
}

func (p Ptr) IsUint64() bool {
	if !p.IsRat() {
		return false
	}
	r := p.RatValue()
	if !r.IsInt() {
		return false
	}
	return r.Num().IsUint64()
}

func (p Ptr) StringValue() string {
	if !p.IsString() {
		panic("not a string literal")
	}
	unobscure(&p)
	return string(p.Bits[:p.control()])
}

func (p Ptr) BytesValue() []byte {
	if !p.IsBytes() {
		panic("not a bytes literal")
	}
	unobscure(&p)
	return p.Bits[:p.control()]
}

func (p Ptr) IsUUID() bool {
	return p.Kind() == LiteralUUID
}

func (p Ptr) UUIDValue() [16]byte {
	unobscure(&p)
	var out [16]byte
	copy(out[:], p.Bits[:16])
	return out
}

func UUID(uuid [16]byte) (p Ptr) {
	copy(p.Bits[:16], uuid[:])
	p.setkind(LiteralUUID)
	obscure(&p)
	return
}

func Bytes(src []byte) (p Ptr) {
	if len(src) > MaxStringLength {
		panic("too many bytes")
	}
	copy(p.Bits[:], src)
	p.setkind(LiteralBytes)
	p.setcontrol(uint16(len(src)))
	obscure(&p)
	return
}

func String(src string) (p Ptr) {
	if len(src) > MaxStringLength {
		panic("string too long")
	}
	copy(p.Bits[:], src)
	p.setkind(LiteralString)
	p.setcontrol(uint16(len(src)))
	obscure(&p)
	return
}

// CanRat returns true if Rat() will not panic on this argument
func CanRat(x *big.Rat) bool {
	if x.IsInt() {
		src := x.Num().Bytes()
		return len(src) <= 30
	} else {
		num := x.Num().Bytes()
		den := x.Denom().Bytes()
		return len(num)+len(den) <= 30
	}
}

func Rat(x *big.Rat) (p Ptr) {
	var nbytes, dbytes int
	neg := x.Sign() == -1

	if x.IsInt() {
		src := x.Num().Bytes()

		if len(src) > 30 {
			panic("int is too big, max is 30 bytes")
		}
		copy(p.Bits[:], src)
		nbytes = len(src)
	} else {
		num := x.Num().Bytes()
		den := x.Denom().Bytes()
		if len(num)+len(den) > 30 {
			panic("rat is too big, max is 30 bytes")
		}
		nbytes = len(num)
		dbytes = len(den)

		copy(p.Bits[:nbytes], num)
		copy(p.Bits[nbytes:], den)
	}
	ctl := uint16(nbytes) << 5
	ctl += uint16(dbytes)
	if neg {
		ctl |= 1 << 10
	}
	p.setcontrol(ctl)
	p.setkind(LiteralRat)
	obscure(&p)
	return
}

func Int64(x int64) (p Ptr) {
	return Rat(big.NewRat(x, 1))
}

func Uint64(x uint64) (p Ptr) {
	var r big.Rat
	r.SetUint64(x)
	return Rat(&r)
}

/*
var bits uint64
var float float64

float := math.Float64frombits(bits)

bits := math.Float64bits(float)
*/

func (p Ptr) LiteralString() string {
	if !p.IsLiteral() {
		return ""
	}
	switch p.Kind() {
	case LiteralString:
		return strconv.Quote(p.StringValue())
	case LiteralBytes:
		return fmt.Sprintf("[%x]", p.BytesValue())
	case LiteralUUID:
		u := p.UUIDValue()
		// This is correct; it is checked by TestUUID
		return fmt.Sprintf("%08x-%04x-%04x-%04x-%08x",
			u[0:4],
			u[4:6],
			u[6:8],
			u[8:10],
			u[10:16])
	case LiteralRat:
		r := p.RatValue()
		return r.RatString()
	default:
		panic("literal value, but unknown kind of literal")
	}
}

// GoString implements GoStringer.  Used for the %#v formatter
func (p Ptr) GoString() string {
	if p.IsLiteral() {
		return p.LiteralString()
	} else {
		return "{" + p.String() + "}"
	}
}
