package ptr

/*
h=bits of hash (total 242 bits of hash data)
s=string or byte data

 Bits[30]   Bits[31]
---- ----  ---- 0000    Zero (NULL)
hhhh hhhh  hhhh 0001  u Untyped
hhhh hhhh  hhhh 0010  t HasTypePointer
hhhh hhhh  hhhh 0011  h Hash
---- ----  ---- 0100
Snnn nndd  ddd0 0101  n Literal/Rat (S=sign, n=numer, d=denom)
ssss ssss  nnnn 0110  b Literal/Bytes (even length 0-30 bytes)
ssss ssss  nnnn 0111  s Literal/String (even length 0-30 bytes)

---- ----  ---- 1000  i Literal/UUID
---- ----  ---- 1001
---- ----  ---- 1010
---- ----  ---- 1011
---- ----  ---- 1100
---- ----  ---- 1101
ssss ssss  nnnn 1110  B Literal/Bytes (odd length 1-31 bytes)
ssss ssss  nnnn 1111  S Literal/String (odd length 1-31 bytes)
*/

import (
	"bytes"
)

const Size = 32

type Ptr struct {
	Bits [Size]byte
}

var Null = Ptr{}

// IsNull checks to see if this pointer is [a/the] null pointer.  The
// "a"/"the" distinction arises from the fact that we only check the
// kind bits, so if other bits are set elsewhere then we will indicate
// *a* IsNull even though it is not *the* null.  But in principle that
// should never happen, the only allowable invalid Ptr *is* *the*
// Null.  But hey, with corruption pretty much anything can happen.
// Note that we will never parse a string for which that is true,
// because we check for valid kind codes
func (p Ptr) IsNull() bool {
	return p.Kind() == 0
}

// note that Null does not count as valid
func (p Ptr) IsValid() bool {
	k := p.Kind()
	return mtEncoding[k] != 0
}

func (k Kind) IsLoadable() bool {
	switch k {
	case Untyped, HasTypePointer, Encrypted:
		return true
	}
	return false
}

func (p Ptr) IsLoadable() bool {
	return p.Kind().IsLoadable()
}

func (p Ptr) IsUntyped() bool {
	return p.Kind() == Untyped
}

func (p Ptr) IsTyped() bool {
	return p.Kind() == HasTypePointer
}

func (p Ptr) IsLiteral() bool {
	switch p.Kind() {
	case LiteralRat, LiteralUUID, LiteralBytes, LiteralString:
		return true
	default:
		return false
	}
}

func (p Ptr) IsHash() bool {
	return p.Kind() == Hash
}

type ErrNotPresent Ptr

func (err ErrNotPresent) Error() string {
	return "object does not exist"
}

func IsNotPresent(err error) bool {
	_, ok := err.(ErrNotPresent)
	return ok
}

func (a Ptr) Less(b Ptr) bool {
	return bytes.Compare(a.Bits[:], b.Bits[:]) < 0
}
