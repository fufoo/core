package tabular

import (
	"bytes"
	"io"
	"unicode/utf8"
)

type Align int

const (
	AlignLeft = Align(iota)
	AlignCenter
	AlignRight
)

type ColumnFormat struct {
	Align    Align
	PadLeft  string
	PadRight string
}

type colInfo struct {
	ColumnFormat
	left, right *fragmenter
}

type fragmenter struct {
	spaceBefore int
	text        string
	spaceAfter  int
}

func toFragmenter(s string) *fragmenter {
	if s == "" {
		return nil
	}
	var sb, sa int
	for len(s) > 0 && s[0] == ' ' {
		sb++
		s = s[1:]
	}
	for len(s) > 0 && s[len(s)-1] == ' ' {
		sa++
		s = s[:len(s)-1]
	}
	return &fragmenter{
		spaceBefore: sb,
		text:        s,
		spaceAfter:  sa,
	}
}

type linebuf struct {
	spaces []int
	texts  []string
	serz   bytes.Buffer
}

func (lb *linebuf) appendSpace(n int) {
	k := len(lb.spaces) - 1
	lb.spaces[k] += n
}

func (lb *linebuf) appendString(s string) {
	lb.appendFragmenter(toFragmenter(s))
}

func (lb *linebuf) appendFragmenter(f *fragmenter) {
	if f == nil {
		return
	}
	if f.text == "" {
		lb.spaces[len(lb.spaces)-1] += f.spaceBefore + f.spaceAfter
		return
	}
	lb.spaces[len(lb.spaces)-1] += f.spaceBefore
	lb.texts = append(lb.texts, f.text)
	lb.spaces = append(lb.spaces, f.spaceAfter)
}

func Write(dst io.Writer, cells [][]string, cols []ColumnFormat) (int, error) {
	if len(cells) == 0 {
		return 0, nil
	}

	widths := make([]int, 0, len(cells[0])) // good guess #cols = cols in first row
	ci := make([]colInfo, len(cols))

	for i, col := range cols {
		ci[i] = colInfo{
			ColumnFormat: col,
			left:         toFragmenter(col.PadLeft),
			right:        toFragmenter(col.PadRight),
		}
	}

	// go through and find the num columns and their widths
	for _, row := range cells {
		for j, cell := range row {
			if j >= len(widths) {
				widths = append(widths, 0)
			}
			w := utf8.RuneCountInString(cell)
			if w > widths[j] {
				widths[j] = w
			}
		}
	}

	total := 0

	// now dump out the data
	var lb linebuf

	for _, row := range cells {
		// zero out the linebuf arrays
		lb.spaces = lb.spaces[:0]
		lb.texts = lb.texts[:0]

		// should always be one more space than text
		lb.spaces = append(lb.spaces, 0)

		for j, cell := range row {
			w := utf8.RuneCountInString(cell)

			var rs int
			if j < len(ci) {
				lb.appendFragmenter(ci[j].left)

				switch ci[j].Align {
				case AlignLeft:
					rs = widths[j] - w
				case AlignRight:
					lb.appendSpace(widths[j] - w)
				case AlignCenter:
					rs = (widths[j] - w) / 2
					lb.appendSpace(widths[j] - w - rs)
				}
			} else {
				// by default, act like left alignment
				rs = widths[j] - w
			}

			lb.appendString(cell)
			lb.appendSpace(rs)

			if j < len(ci) {
				lb.appendFragmenter(ci[j].right)
			}
		}
		lb.WriteTo(dst)
	}
	return total, nil
}

var spaces = []byte("                    ")

func (lb *linebuf) WriteTo(dst io.Writer) (int64, error) {
	lb.serz.Reset()

	// skip the last space (that's part of the whole point of this)
	for i, spc := range lb.spaces[:len(lb.spaces)-1] {
		for spc > 0 {
			n := spc
			if n > 20 {
				n = 20
			}
			lb.serz.Write(spaces[:n])
			spc -= n
		}
		lb.serz.WriteString(lb.texts[i])
	}
	lb.serz.WriteByte('\n')
	return lb.serz.WriteTo(dst)
}
